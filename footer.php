<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package brainfuel2017
 */

?>

	</div><!-- #content -->

	<footer class="site-footer">
		<div class="site-content">
			<div class="left">
				<h2>Stay up to date</h2>
				<p>Please fill out the fields down below if you want to receive a notification when Brain Fuel is available. Don’t worry: we will do our best not to spam you :)</p>
				<br/>
				<?php echo do_shortcode('[mc4wp_form id="17"]'); ?>
			</div>
			<div class="right">
				<iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fbrainfueltool&tabs&width=340&height=130&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=false&appId=648411538701610" width="340" height="130" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
				<br/><br/>
				<h4>Contact us</h4>
				info@brainfueltool.com<br/>
				+31(0)58 - 203 80 76<br/>
				<div class="credits">
					<span class="creditsText">A project by </span><a href="https://www.onlinemediakoffer.nl" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/OMK.svg"/></a> <span class="credits">and</span> <a href="https://www.gist.nl" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/gist.svg"/></a>
				</div>
			</div>
		</div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>
<!-- Global Site Tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-106720297-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments)};
  gtag('js', new Date());

  gtag('config', 'UA-106720297-1');
</script>


</body>
</html>
