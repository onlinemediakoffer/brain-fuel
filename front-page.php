<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 * Template Name: homepage
 * @package brainfuel2017
 */

get_header(); ?>
<div id="primary" class="content-area">
	<section class="videoHeader unboxed">
		<div class="left">
			<?php
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/content', 'page' );

				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;

			endwhile; // End of the loop.
			?>
		</div>
		<div class="right">
			<iframe src="https://player.vimeo.com/video/236552918" width="640" height="295" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
		</div>
	</section>
	<section class="boxed">
		<div class="container">
			<div class="left">
				<h2>Meet Brain Fuel</h2>
				<p>
				Our mission is to ensure that as many ideas as possible change into good ideas. We break through standard thought patterns, so you can turn your normal ideas into great ones!
				<br/><br/>
				So, are you an innovator, teacher, strategist, entrepreneur, designer, student, marketeer, maker, humanitarian.. or anyone who wants or needs to solve a problem in a creative way? Brain Fuel is your answer.
				</p>
			</div>
			<div class="right nopadding meetBrainFuel">
				
			</div>
		</div>
	</section>
	<section class="boxed">
		<div class="container">
			<div class="left">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/categories.png" />
			</div>
			<div class="right">
				<h2>Choose from 4 categories</h2>
				<p>With Brain Fuel you work with 100 association cards in the following 4 categories:<br/><br/>
				1. Animals<br/>
				2. Characters <br/>
				3. Locations <br/>
				4. Objects <br/><br/>
				Some categories work perfectly with one brainstorm technique, some categories work well with others. Maybe we will add even more categories in the future (who are we kidding, of course we will).<br/>
				</p>
			</div>
		</div>
		<div class="wideTitle">
			<h2>Choose your brainstorm method</h2>
		</div>
		<div class="container">
			<div class="left noPaddingBottom">
				<p>To kickstart your brainstorm we've added 10 different methods that you can use. Some of them classics, others brand new.
				</p>
			</div>
			<div class="right noPaddingBottom">
				<p>
					The methods all vary in terms of creativity and difficulty. Yes, Brain Fuel is for both brainstorm-beginners and brainstorm-experts. In addition to that, we will continue to add new methods in the future!
				</p>
			</div>
		</div>
		<div class="container">
			<div class="wide">
				<ul class="methodesList">
					<li><a href="#apples-and-oranges" class="fancybox">
						<div class="image">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/applesoranges-black.svg"/>
						</div>
						<h3>Apples and oranges </h3>
					</a></li>
					<div class="fancybox-hidden fancyContent" style="display: none;">
						<div id="apples-and-oranges">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/applesoranges-black.svg"/>
							<h2>Apples and oranges </h2>
							<span class="smallText">Difficulty: Easy</span>
							<p>
								<strong>Target destination</strong><br/>
								It helps to get to know each other better before brainstorming (it kinda helps to get to know each other in general). Apples and oranges is an easy icebreaker exercise. 
								<br/><br/>
								<Strong>Journey</Strong><br/>
								Let every participant draw a card from the deck or swipe a card in the app. Ask them to think of an attribute they have in common with the card in question. And let everyone introduce themselves with what they have in common with the card and why. This exercise helps making unusual associations that warm up the brain for the brainstorm. Bonus: it will give you some surprising insights into other participants that you don’t get when just asking for their name and occupation. 
								<br/><br/>
								<Strong>For example</Strong><br/>
								Harry draws a card of ‘Barack Obama’ and introduces himself: Hello, my name is Harry and I am a student at Hogwarts. Just like Obama I want to do the right thing and make the world a better place. Hugh draws a card of ‘Buddha’ and says: Hi, I’m Hugh and I am a publisher of a lifestyle magazine. Just like Buddha I prefer not to worry. I chill.
							</p>
						</div>
					</div>
					<li><a href="#once-upon-a-time" class="fancybox">
						<div class="image">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/onceupon-black.svg"/>
						</div>
						<h3>Once upon a time</h3>
					</a></li>
					<div class="fancybox-hidden fancyContent" style="display: none;">
						<div id="once-upon-a-time">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/onceupon-black.svg"/>
							<h2>Once upon a time</h2>
							<span class="smallText">Difficulty: Easy peasy</span>
							<p>
								<strong>Target destination</strong><br/>
								Have some fun warming up your brain by telling each other strange stories. Simply use a couple of cards and start connecting a character, an object, an animal and a location!
								<br/><br/>
								<Strong>Journey</Strong><br/>
								Start by laying the four stacks of cards face down; make the first stack ‘Characters’, the second ‘Objects’, the third ‘Animals’ and the fourth ‘Locations’. Turn over one card from each stack; and start creating a story! If you want to use two characters, no objects and fifteen animals, or use a different order: be our guest. It is your story! 
								<br/><br/>
								<Strong>For example</Strong><br/>
								Suppose you draw the alien, a tree, a cow and Russia. You could come up with: The story goes that an alien wanted to visit Russia but crash landed on the wrong side of the border. So the alien hid itself in a tree for quite some time until a cow walked past. Since the alien knows everything there is to know about cows, the alien takes over the body of the cow. The guards of the Russian border patrol never noticed the cow crossing the border. Then the alien traveled all of Russia in the body of the cow. When it got bored it went to Baikonur, hijacked a rocket and launched itself to the moon. True story.
							</p>
						</div>
					</div>
					<li><a href="#association-chain" class="fancybox">
						<div class="image">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/associationchain-black.svg"/>
						</div>
						<h3>Association chain</h3>
					</a></li>
					<div class="fancybox-hidden fancyContent" style="display: none;">
						<div id="association-chain">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/associationchain-black.svg"/>
							<h2>Association chain</h2>
							<span class="smallText">Difficulty: Easy peasy</span>
							<p>
								<strong>Target destination</strong><br/>
								Fuel your brain. Get everybody in a creative-thinking state of mind!
								<br/><br/>
								<Strong>Journey</Strong><br/>
								Swipe to get a random card and make sure everyone can see it. Ask the group: “What kind of associations do you get from this card? Write the first one down and keep on associating, so you will get a chain of associations.
								<br/><br/>
								<Strong>For example</Strong><br/>
								Let’s say the card you get is the treasure chest card. What does this make you think of? This could be your association chain: treasure chest -> pirate -> parrot -> zoo ->  and so on!
							</p>
						</div>
					</div>
					<li><a href="#superhero" class="fancybox">
						<div class="image">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/superhero-black.svg"/>
						</div>
						<h3>Superhero</h3>
					</a></li>
					<div class="fancybox-hidden fancyContent" style="display: none;">
						<div id="superhero">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/superhero-black.svg"/>
							<h2>Superhero</h2>
							<span class="smallText">Difficulty: Easy peasy</span>
							<p>
								<strong>Target destination</strong><br/>
								Walk a mile in the shoes of a superhero! Discover how they would solve your problems.
								<br/><br/>
								<Strong>Journey</Strong><br/>
								For this form you normally only use the “People” cards. Swipe to a random person card. <br/>Write down all his or her qualities. Put yourself in his or her shoes: How would this person solve your problem?<br/> If you a brainstorming pro, you can also try to use the animals or maybe even the locations!
								<br/><br/>
								<Strong>For example</Strong><br/>
								Imagine your company needs a new interior. How would Don Corleone fix this? And what about Albert Einstein?
							</p>
						</div>
					</div>
					<li><a href="#brainwriting" class="fancybox">
						<div class="image">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/brainwriting-black.svg"/>
						</div>
						<h3>Brainwriting</h3>
					</a></li>
					<div class="fancybox-hidden fancyContent" style="display: none;">
						<div id="brainwriting">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/brainwriting-black.svg"/>
							<h2>Brainwriting</h2>
							<span class="smallText">Difficulty: Easy</span>
							<p>
								<strong>Target destination</strong><br/>
								Explore many threads in high speed! Ideal for teams with a couple of quiet people. Or teams with people who are better at improving someone else's ideas rather than thinking up their own. Yes, we mean every team ever. Fun fact: this method has been proven scientifically!
								<br/><br/>
								<Strong>Journey</Strong><br/>
								Set a timer and let each participant note down one idea per post-it. Let them slide the post-its to the person at their right after a few minutes (or left, whatever you want). Reset the timer and add your own ideas to the ideas you received from someone else. Repeat this process until you receive your own post-its again - now with added ideas from everybody in the team!<br/><Br/>
								If you notice that people need some extra inspiration, give them a Brain Fuel card. You can do that at the beginning of the brainstorm, or in between. Give everyone the same ticket or everybody a different one. Decisions, decisions! Ask the team if they want to think up one idea that comes with an association from the card. Push them a little (but not too much)!
								<br/><br/>
								<Strong>For example</Strong><br/>
								What if your restaurant is not running well and you want to brainstorm about a solution? <br/>
								<Br/>Thanks to the keyhole-card you write ‘we need to make our restaurant mysterious’ on a post-it. The person to the right of adds -with credits to the haunted house card-: exciting theme nights like a murder mystery game! And even further to the right: organize a dinner in a secret location!   <Br/><Br/>
								See? Other people can improve on your idea. Or at least devise some different ones! 
							</p>
						</div>
					</div>
					<li><a href="#Associaton-machine-gun" class="fancybox">
						<div class="image">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/machinegun-black.svg"/>
						</div>
						<h3>Associaton machine gun</h3>
					</a></li>
					<div class="fancybox-hidden fancyContent" style="display: none;">
						<div id="Associaton-machine-gun">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/machinegun-black.svg"/>
							<h2>Associaton machine gun</h2>
							<span class="smallText">Difficulty: Easy</span>
							<p>
								<strong>Target destination</strong><br/>
								The association machine gun is ideal when your group is already warmed up or when your group is experienced in creative thinking. The association machine gun is based on the premise that any word and image can lead to new associations. And when you apply these associations to your problem, they can make amazing new ideas!
								<br/><br/>
								<Strong>Journey</Strong><br/>
								Pick one card. Call out a few associations the card gives you. Use these associations to think up new ideas for your problem. The funnier, the better!<br/>
								<br/>
								Does the speed in which ideas pop up in your mind slow down? Then it's time to pick an other card. Next! 
								<br/><br/>
								<Strong>For example</Strong><br/>
								Imagine it's too busy at work (maybe you don’t need that much imagination for this…). The first card you pull is a mouse. Maybe you could hide yourself! Or make a sad peeping sound to bring pity to your colleagues. Next card: a sword! Perhaps you need to cut your job in two. Or you cut off the minor elements of an assignment so you have more time left for the important stuff. Another card! Cinderella. Perhaps you have to make sure you have your own Cinderella, someone who does all those terrible tasks for you (that makes you the evil stepmother, but oh well).<br/>
								<br/>
								Still not enough ideas? Keep drawing cards!
							</p>
						</div>
					</div>
					<li><a href="#deserted-island" class="fancybox">
						<div class="image">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/desertedisland-black.svg"/>
						</div>
						<h3>Deserted Island</h3>
					</a></li>
					<div class="fancybox-hidden fancyContent" style="display: none;">
						<div id="deserted-island">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/desertedisland-black.svg"/>
							<h2>Deserted Island</h2>
							<span class="smallText">Difficulty: Normal</span>
							<p>
								<strong>Target destination</strong><br/>
								Think from the point of view of an object and discover new solutions to your problem.
								<br/><br/>
								<Strong>Journey</Strong><br/>
								For this form you normally only use the “People” cards. Swipe to a random person card. <br/>Write down all his or her qualities. Put yourself in his or her shoes: How would this person solve your problem?<br/> If you a brainstorming pro, you can also try to use the animals or maybe even the locations!
								<br/><br/>
								<Strong>For example</Strong><br/>
								Imagine your company needs a new interior. How would Don Corleone fix this? And what about Albert Einstein?
							</p>
						</div>
					</div>
					<li><a href="#brainfusion" class="fancybox">
						<div class="image">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/brainfusion-black.svg"/>
						</div>
						<h3>Brainfusion</h3>
					</a></li>
					<div class="fancybox-hidden fancyContent" style="display: none;">
						<div id="brainfusion">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/brainfusion-black.svg"/>
							<h2>Brainfusion</h2>
							<span class="smallText">Difficulty: Intermediate</span>
							<p>
								<strong>Target destination</strong><br/>
								When you release the power of Brainfusion you risk melting your brain. So use its power wisely! Brainfusion a quick fire association technique which fuses the question you want to solve with our cards. By adding at least two categories of cards all kinds in weird combinations you can find new ideas or polish up previous ones.
								<br/><br/>
								<Strong>Journey</Strong><br/>
								Lay out a grid of cards with three cards of each category; a column of three characters, three objects, three locations and three animals. Take your initial question and start adding a random combination of card. Start with two cards, then three and when you feel really inspired add four cards to the starter question. Just start spawning new hybrid ideas till your power runs out.
								<br/><br/>
								<Strong>For example</Strong><br/>
								Let’s say your starter question is: how can we improve our customer service? Add two cards and change the question to ‘how might we improve our customer service in our candy house for Cleopatra’? Add a object, like: how can Cleopatra use a robot to improve our customer service in our candy house? And yes, even 4 additives to your initial question will deliver more ideas. And guess what.. you can even change the previous cards! How can a jelly fish improve our customer service for a baby in a volcano using a tree? Now that will get you some strange and specific solutions!
							</p>
						</div>
					</div>
					<li><a href="#analogy" class="fancybox">
						<div class="image">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/analogy-black.svg"/>
						</div>
						<h3>Analogy</h3>
					</a></li>
					<div class="fancybox-hidden fancyContent" style="display: none;">
						<div id="analogy">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/analogy-black.svg"/>
							<h2>Analogy</h2>
							<span class="smallText">Difficulty: Hard</span>
							<p>
								<strong>Target destination</strong><br/>
								Compare two concepts. Their differences and similarities will help you come up with new ideas.
								<br/><br/>
								<Strong>Journey</Strong><br/>
								Name your problem in a single keyword and swipe to get a random card. Option 1: which qualities of your card are different than the qualities of your keyword? Write this out. Combine these with your problem and discover new solutions. Option 2: What do your card and your keyword have in common? Write this down and again, come up with new solutions!
								<br/><br/>
								<Strong>For example</Strong><br/>
								Let’s say the keyword of your problem is chair and your card is elephant. A quality of an elephant is its long snout. Chair + long snout = a chair with a build-in vacuum cleaner? You can also sit on both a chair and an elephant. Chair + elephant = a table with a seat in it?
							</p>
						</div>
					</div>
					<li><a href="#hit" class="fancybox">
						<div class="image">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hit-black.svg"/>
						</div>
						<h3>HIT!</h3>
					</a></li>
					<div class="fancybox-hidden fancyContent" style="display: none;">
						<div id="hit">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hit-black.svg"/>
							<h2>HIT!</h2>
							<span class="smallText">Difficulty: Hardcore</span>
							<p>
								<strong>Target destination</strong><br/>
								Combine two concepts that, at first sight, seem to have nothing to do with each other. A big bang for your brain!
								<br/><br/>
								<Strong>Journey</Strong><br/>
								Swipe to get a random card and write down four features that are specific to that card. Now write down a list of four features of the problem you are trying to solve. Create a grid with at the top of each column a feature of the card. In front of every row, write down the features of your assignment. Create an idea for each combination. Write this down on a post-it and stick it on the cell where it should be.
								<br/><br/>
								<Strong>For example</Strong><br/>
								Let’s say the card you get is the pocketknife card. Four features of a pocketknife are<br/>
								1. multifunctiona<br/>
								2. compact<br/>
								3. sharp<br/>
								4. Swiss<br/>
								<br/>
								Let’s say your assignment is to make sure more people will use solar panels to get their power from. Features of this problem are<br/>
								1. installing costs,<br/>
								2. permits,<br/>
								3.looks,<br/>
								4. profitable.<br/>
								<br/>
								Start with an easy or existing combination, like compact and profitable (= for example a mobile charger powered by the sun). Force yourself to come up with ideas for the less obvious combinations (multifunctional + licenses = ?). Hit it!
								<br/><br/>
								<strong>Tip</strong>
								Use two Brain Fuel cards. Combine these features from the features of your assignment and you will be able to come up with even more unique ideas!
							</p>
						</div>
					</div>
				</ul>
			</div>
		</div>
	</section>
	<section class="unboxed">
		<div class="mid">
			<h2>Choose between an app or the cards</h2>
			<p>We are there for lovers of technology and lovers of paper! The card game is tailormade for brainstorms with big groups and with the app you'll always have Brain Fuel nearby for a quick and powerful brainstorm!
			</p>
		</div>
		<div class="chooseImg">
			<img class="choose" src="<?php echo get_stylesheet_directory_uri(); ?>/images/iphoneEnKaarten-2.png"/>
		</div>
	</section>
	<section class="boxed">
		<div class="container">
			<div class="left">
				<h2>Proof of concept</h2>
				<p>
				One simply doesn’t develop a tool out of thin air. Research, testing, more research, more tests, and so forth and so on! As of now, more than 350 people have tested Brain Fuel. Most of them live in our homeland, the Netherlands.. but we were also very honoured that people from Finland, Germany, the United States, Lithuania, Sweden, Thailand, Turkey, the United Kingdom, Chili, Macedonia, Kenya and more got to test Brain Fuel. We could not have done this without you! 
				</p>
			</div>
			<div class="right">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/brainfuel_wereldkaart_wit.gif" />
			</div>
		</div>
		<div class="container">
			<div class="left nopadding people">
				<div class="row">
					<div class="one"></div>
					<div class="two"></div>
				</div>
				<div class="row hide">
					<div class="three"></div>
					<div class="four"></div>
				</div>
				<div class="row hide">
					<div class="five"></div>
					<div class="six"></div>
				</div>
			</div>
			<div class="right">
				<h2>Others said this</h2>
				<p>
					<span class="stars"><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i></span>
					<quote><strong>Speed, humor and energy</strong><br/>
					I used Brain Fuel for a session with my team, in which we only had 1,5 hours to brainstorm about two major themes. The method brings speed, humor and energy into the group, which gives you a great harvest of ideas in a relatively short time. And in a fun way!</quote>
					Rixt Sijtsma - Supervisor Aegon<br/>
					<br/>
					<span class="stars"><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i></span>
					<quote><strong>A great concept in no time at all</strong><br/>
					By combining random stuff like an Pineapple and Marilyn Monroe with the problem we tried to solve, we thought up a great concept in no time at all. Because Brain Fuel forced us to associate with things we do not usually think about, we thought up something we could never think of ourselves!</quote>
					Jorik Kleen - Student Communication & Multimedia Design
				</p>
			</div>
		</div>
	</section>
	<!-- <section class="unboxed">
		<div class="mid">
			<h2>Help us: back us!</h2>
			<p>When we came to the conclusion that we invented something that did not exist yet and in which we truly believe, we thought: why keep this to our ourselves? Our industry? Of even our country? Why not share it with as many people as possible? And that is exactly what we are going to do.
			<br/>Kickstarter is a perfect tool for us to help us raise money to print the cards and packaging, but also to reach an international audience!<br/><br/>
			<div class="backButton">
				<a href="http://omk.frl/brainfuel" class="btn" target="_blank">Fund on kickstarter</a>
			</div>
			</p>
			<img class="alien" src="<?php echo get_stylesheet_directory_uri(); ?>/images/Alien-button.png" />
		</div>
	</section> -->
</div>
	
<?php
get_footer();
