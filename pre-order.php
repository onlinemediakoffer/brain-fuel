<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 * Template Name: pre order
 * @package brainfuel2017
 */

get_header(); ?>
<div id="primary" class="content-area">
	<section class="videoHeader boxed">
		<div class="container">
			<div class="left">
				<?php
				while ( have_posts() ) : the_post();

					get_template_part( 'template-parts/content', 'page' );

					// If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;

				endwhile; // End of the loop.
				?>
			</div>
			<div class="right">
				<img class="choose" src="<?php echo get_stylesheet_directory_uri(); ?>/images/iphoneEnKaarten-2.png"/>
			</div>
		</div>
	</section>
</div>
	
<?php
get_footer();
