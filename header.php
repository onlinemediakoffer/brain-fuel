<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package brainfuel2017
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/favicon.png" type="image/x-icon">

	<?php wp_head(); ?>
	<link href="https://fonts.googleapis.com/css?family=Quicksand:400,700" rel="stylesheet">
	<script src="https://use.fontawesome.com/36cecde8d0.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
		    $("body").children().each(function() {
		        $(this).html($(this).html().replace(/&#8232;/g," "));
		    });
		});
	</script>

</head>

<body <?php body_class(); ?>>
<div id="background_wrap"></div>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'brainfuel2017' ); ?></a>

	<header id="masthead" class="site-header">
		<div class="site-branding">
			<a href="<?php echo site_url(); ?>" title="Terug naar de homepage"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/Brain-Fuel-logo.svg"/></a>
		</div><!-- .site-branding -->
		<a style="color:#ffffff;" href="https://www.indiegogo.com/projects/brain-fuel-a-unique-ideation-tool#/" target="_blank" class="btn header">Pre-order now</a>

	</header><!-- #masthead -->

	<div id="content" class="site-content">
